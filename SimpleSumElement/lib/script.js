function GetDeleteButton(row) {
	return "<button id='" + row + "' class='btn btnDelete' onclick='RemoveRow(\"" + row +"\")'><spam class='glyphicon glyphicon-remove'></span></button>";
}

function GetElement(row, type, options) {
	if (options == undefined) {
		options = {
			name : "default-name[]",
			id : "default-id",
			class : "default-class"
		};
	}
	
	var str_elem = "";
	
	switch(type) {
		case "label-hidden":
			str_elem = 
				"<span class='label-" + options.class + "' id='label-" + options.id + row + "'></span>" +
				"<input type='hidden' class='form-control input-sm input-" + options.class + "' id='input-" + options.id + row + "' name='" + options.name + "' value='' />";
			break;
		case "label":
			str_elem = "<span class='" + options.class + "' id='" + options.id + row + "'></span>";
			break;
		case "number":
			str_elem = "<input type='number' class='form-control input-sm " + options.class + "' id='" + options.id + row + "' name='" + options.name + "' value='' />";
			break;
		default:
			str_elem = "<input type='text' class='form-control input-sm " + options.class + "' id='" + options.id + row + "' name='" + options.name + "' value='' />";
	}
	
	
	return str_elem;
}


function RemoveRow(row) {
	jQuery("#row_" + row).remove();
	SumElement("total-price","total");
}

function SumElement(selector_source_elem, selector_destination_elem) {
	var total = 0;
	
	jQuery("." + selector_source_elem).each(function() {
		var val_elem = jQuery(this).val();
		var html_elem = jQuery(this).html();
		if (val_elem != undefined && val_elem != "") {
			total += parseFloat(val_elem);
		} else if (html_elem != undefined && html_elem != "") {
			total += parseFloat(html_elem);
		}
	});
	
	jQuery("#" + selector_destination_elem).val(total);
	jQuery("#" + selector_destination_elem).html(total);
}

var row = 0;
jQuery("#btnAddRow").click(function() {
	var options = {
		name : "price[]",
		id : "price",
		class : "price"
	};
	
	var str_elem = "<td>" + GetElement(row, "number", options) + "</td>";
	
	options = {
		name : "qty[]",
		id : "qty",
		class : "qty"
	};
	
	str_elem += "<td>" + GetElement(row, "number", options) + "</td>";
	
	options = {
		name : "total-price[]",
		id : "total-price",
		class : "total-price"
	};
	
	str_elem += "<td>" + GetElement(row, "label", options) + "</td>";
	
	var str_button = "<td>" + GetDeleteButton(row)+ "</td>";
	var row_elem = "<tr id='row_" + row +"'>" + str_elem + str_button + "</tr>";
	
	jQuery("#form").append(row_elem);
	
	var row_elem = row;
	jQuery("#qty" + row_elem).on("input", function() {
		var qty = $(this).val();
		if (qty == "") qty = 0;
		var price = $("#price" + row_elem).val();
		if (price == "") price = 0;
		jQuery("#total-price" + row_elem).html(qty * price);
		SumElement("total-price","total");
	});
	
	jQuery("#price" + row).on("input", function() {
		var qty = $("#qty" + row_elem).val();
		if (qty == "") qty = 0;
		var price = $(this).val();
		if (price == "") price = 0;
		jQuery("#total-price" + row_elem).html(qty * price);
		SumElement("total-price","total");
	});
	
	row++;
});

