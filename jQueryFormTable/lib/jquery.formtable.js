(function($) {
	$.fn.FormTable = function(options) {
		var form_table = new FormTable();
		var button_id = (options != undefined ? (options.button != undefined ? options.button : "btnAddRow") : "btnAddRow");
		var table_id = $(this).attr("id");
		
		form_table.Init($(this), options);
		$("#" + button_id).click(function() {
			form_table.AddRow();
		});
		
		jQuery(document).on("click", ".btnDelete_" + table_id, function() {
			var row_id = jQuery(this).closest("tr").attr("id");
			form_table.DeleteRow(row_id);
		});
		
		return form_table;
	};
	
	function FormTable() {
		this.form_options = {
			name : "",
			type : "",
			value : "",
			attributes : []
		};
		
		this.table_options = {
			removeButton : true
		}
		
		this.form_elements = Array();
		this.index_element = 0;
		this.table_element = null;
		this.row_table = 0;
		this.table_id = null;
		FormTable.prototype.Init = function(table, options) {
			this.table_element = table;
			this.table_id = $(this.table_element).attr("id");
			if (options != undefined) { this.table_options = options; }
		}
		
		FormTable.prototype.RegisterElement = function(options) {		
			if (options == undefined) { options = this.form_options; }
			this.form_elements[this.index_element] = options;
			this.index_element++;
		}
		
		FormTable.prototype.AddRow = function() {
			var tbody = $(this.table_element).find("tbody");
			var generated_element = this.GenerateElement();
			this.row_table++;
			$(tbody).append(generated_element);
		}

		FormTable.prototype.DeleteRow = function(row) {
			if (row != undefined) {
				 jQuery("#" + row).remove();
			}
		}
		
		FormTable.prototype.GenerateElement = function() {
			var element_text = "";
			for(item in this.form_elements) {
				if (this.form_elements[item] == undefined) { continue; }
				element_text += "<td>" + this.ElementChooser(this.form_elements[item]) + "</td>";
			}
			if (this.table_options.removeButton) {
				element_text += "<td>" + this.ElementChooser({type : "deletebutton"}) + "</td>";
			}
			if (element_text.search("{index}") > -1) {
				element_text = this.StringReplace(element_text, "{index}", this.row_table);
			}
			return "<tr id='row_" + this.table_id + "_" + this.row_table + "'>" + element_text + "</tr>";
		}
		
		FormTable.prototype.ElementChooser = function(options) {
			var element_text = "";
			if (options.type != undefined) {
				switch(options.type) {
					case "textbox" :
						element_text = this.GenerateTextBox(options);
						break;
					case "checkbox" :
						element_text = this.GenerateCheckBox(options);
						break;
					case "number" :
						element_text = this.GenerateNumber(options);
						break;
					case "deletebutton" :
						element_text = this.GenerateDeleteButton(options);
						break;
					default:
						element_text = this.GenerateTextBox(options);
				}
			}
			
			return element_text;
		}
		
		
		FormTable.prototype.GenerateTextBox = function(options) {
			var attributes = "";
			if (options.attributes != undefined) {
				for(item in options.attributes) {
					if (options.attributes[item] == undefined) { continue; }
					var default_attributes_val = this.GetDefaultAttributesValue(options.type, item);
					attributes += " " + item + "='" + default_attributes_val + options.attributes[item] + "'";
				}
			}
			
			return "<input type='text' name='" + options.name + "' value = '" + options.value + "' " + attributes + " />";
		}

		FormTable.prototype.GenerateCheckBox = function(options) {
			var attributes = "";
			if (options.attributes != undefined) {
				for(item in options.attributes) {
					if (options.attributes[item] == undefined) { continue; }
					var default_attributes_val = this.GetDefaultAttributesValue(options.type, item);
					attributes += " " + item + "='" + default_attributes_val + options.attributes[item] + "'";
				}
			}
			if (options.value) {
				attributes += " checked='checked'";
			}
			return "<input type='checkbox' name='" + options.name + "' value = '" + options.value + "' " + attributes + " />";
		}
		
		FormTable.prototype.GenerateNumber = function(options) {
			var attributes = "";
			if (options.attributes != undefined) {
				for(item in options.attributes) {
					if (options.attributes[item] == undefined) { continue; }
					var default_attributes_val = this.GetDefaultAttributesValue(options.type, item);
					attributes += " " + item + "='" + default_attributes_val + options.attributes[item] + "'";
				}
			}
			
			return "<input type='number' name='" + options.name + "' value = '" + options.value + "' " + attributes + " />";
		}
		
		FormTable.prototype.GenerateDeleteButton = function(options) {
			var attributes = "";
			if (options.attributes != undefined) {
				for(item in options.attributes) {
					if (options.attributes[item] == undefined) { continue; }
					var default_attributes_val = this.GetDefaultAttributesValue(options.type, item);
					attributes += " " + item + "='" + default_attributes_val + options.attributes[item] + "'";
				}
			}
			
			return "<button id='' class='btn btnDelete_" + this.table_id + "' ><span class='glyphicon glyphicon-remove'></span></button>";
		}

		FormTable.prototype.AddData = function(datas) {
			var add_row = false;
			for(item in this.form_elements) {
				if (this.form_elements[item] == undefined) { continue; }
				var obj_options = this.form_elements[item];
				var value = this.BindData(obj_options, datas);
				if (value != null && value != undefined) {
					obj_options.value = value;
				}
				this.form_elements[item] = obj_options;
				add_row = true;
			}
			if (add_row) {
				this.AddRow();
			}
		}

		FormTable.prototype.BindData = function(options, datas) {
			if (datas != null && datas != undefined) {
				for (item_data in datas) {
					if (datas[item_data] == undefined) continue;
					var key = Object.keys(datas[item_data])[0];
					var value = datas[item_data][key];
					if (options.name == key) {
						return value;
					}
				}
			}
			return null;
		}
		
		FormTable.prototype.GetDefaultAttributesValue = function(type, attributes_name) {
			var attributes_val = "";
			if (attributes_name == "class") {
				if (type != "checkbox") {
					attributes_val = "form-control input-sm ";
				}
			}
			return attributes_val;
		}

		FormTable.prototype.StringReplace = function(str, find, replace) {
			return str.replace(new RegExp(find, 'g'), replace);
		}
		
	}
	
}(jQuery))