function GetDeleteButton(row) {
	return "<button id='" + row + "' class='btn btnDelete' onclick='RemoveRow(\"" + row +"\")'><spam class='glyphicon glyphicon-remove'></span></button>";
}

function AddRow(row) {
	var str_elem = "<input type='text' class='form-control input-sm' id='elementText" + row + "' name='elementText[]' value='' />";
	return str_elem;
}

function RemoveRow(row) {
	jQuery("#row_" + row).remove();
}

var row = 0;
jQuery("#btnAddRow").click(function() {
	var str_elem = AddRow(row);
	var str_button = GetDeleteButton(row);
	var row_elem = "<tr id='row_" + row +"'><td>" + str_elem + "</td><td>" + str_button + "</td></tr>"
	jQuery("#form").append(row_elem);
	row++;
});